unit Unit1;

interface

uses
  DateUtils,Shellapi,Clipbrd,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.Menus, Vcl.OleCtrls, SHDocVw, Vcl.StdCtrls, Vcl.CheckLst, Vcl.ComCtrls,
  Vcl.ExtCtrls, cefvcl;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    NOpen: TMenuItem;
    grid: TStringGrid;
    PopupMenu1: TPopupMenu;
    NNew: TMenuItem;
    NDel: TMenuItem;
    PopupMenu2: TPopupMenu;
    GroupBox1: TGroupBox;
    Ncol: TMenuItem;
    NAdd_col: TMenuItem;
    NDel_col: TMenuItem;
    new_col_edit: TEdit;
    pm_otd: TPopupMenu;
    NAdd_otd: TMenuItem;
    btn_otl: TButton;
    NDel_otd: TMenuItem;
    pm_cabinet: TPopupMenu;
    NFile: TMenuItem;
    NSaveFile: TMenuItem;
    NLoadFile: TMenuItem;
    cbCab: TComboBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    rgDate: TRadioGroup;
    GroupBox2: TGroupBox;
    List: TCheckListBox;
    gb: TGroupBox;
    rgDate1: TRadioGroup;
    editCab: TEdit;
    PopupMenu3: TPopupMenu;
    N1: TMenuItem;
    NRefrech_otd: TMenuItem;
    NRefresh: TMenuItem;
    mount: TMonthCalendar;
    btNew: TButton;
    WebBrowser1: TWebBrowser;
    Nhtml: TMenuItem;
    NDIR: TMenuItem;
    NPrint: TMenuItem;
    chAuto: TCheckBox;
    test1: TMenuItem;
    chLastNed: TCheckBox;
    NModif_otd: TMenuItem;
    procedure NOpenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure gridClick(Sender: TObject);
    procedure NAdd_colClick(Sender: TObject);
    procedure NDel_colClick(Sender: TObject);
    procedure NDel_rowClick(Sender: TObject);
    procedure NAdd_otdClick(Sender: TObject);
    procedure btn_otlClick(Sender: TObject);
    procedure NDel_otdClick(Sender: TObject);
    procedure NLoadFileClick(Sender: TObject);
    procedure NSaveFileClick(Sender: TObject);
    procedure checkClick(Sender: TObject);
    procedure gridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure rgDateClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure NRefrech_otdClick(Sender: TObject);
    procedure NRefreshClick(Sender: TObject);
    procedure editCabChange(Sender: TObject);
    procedure ListClick(Sender: TObject);
    procedure mountClick(Sender: TObject);
    procedure rgDate1Click(Sender: TObject);
    procedure btNewClick(Sender: TObject);
    procedure NhtmlClick(Sender: TObject);
    procedure NDIRClick(Sender: TObject);
    procedure NPrintClick(Sender: TObject);
    procedure gridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure chAutoClick(Sender: TObject);
    procedure test1Click(Sender: TObject);
    procedure NModif_otdClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure �����������������(���:String);
    procedure ��������������;
    function ������������(i:Integer):Boolean;
    function �������������(col:Integer;key:Integer):Boolean;
    procedure ��������������(key:Integer);
    procedure ��������;
    procedure �����(col:Integer);
  end;

var
  Form1: TForm1;

implementation
var
myDate:TDate;
myYear, myMonth, myDay:Word;

{$R *.dfm}

// *****************************************
// Load a TStringGrid from a file
procedure LoadStringGrid(Grid: TStringGrid; const FileName: TFileName);
var
  f: TextFile;
  iTmp, i, k: Integer;
  strTemp: String;
begin
  if not FileExists(FileName) then
    exit;

  AssignFile(f, FileName);
  Reset(f);
  with Grid do
  begin
    // Get number of columns
    Readln(f, iTmp);
    ColCount := iTmp;
    // Get number of rows
    Readln(f, iTmp);
    RowCount := iTmp;
    // loop through cells & fill in values
    for i := 0 to ColCount - 1 do
      for k := 0 to RowCount - 1 do
      begin
        Readln(f, strTemp);


        if (i>0) and (k>0) then
        begin
        if (strTemp='') then
            Objects[i,k]:=Pointer(0)
        else
            Objects[i,k]:=Pointer(StrToInt(strTemp));
        end
        else
            Cells[i, k] := strTemp;

      end;
  end;
  CloseFile(f);
  grid.Repaint;
end;

// Save a TStringGrid to a file
procedure SaveStringGrid(Grid: TStringGrid; const FileName: TFileName);
var
  f: TextFile;
  i, k: Integer;
  tex:String;
begin
  AssignFile(f, FileName);
  Rewrite(f);
  with Grid do
  begin
    // Write number of Columns/Rows
    Writeln(f, ColCount);
    Writeln(f, RowCount);
    // loop through cells
    for i := 0 to ColCount - 1 do
      for k := 0 to RowCount - 1 do
      begin
        if (i>0) and (k>0) then
        begin
            tex:=integer(Objects[i,k]).ToString;
        end
        else
            tex:=Cells[i,k];
        Writeln(f, tex);
      end;
  end;
  CloseFile(f);
end;

function ������������: String;
var
  myYear, myMonth, myDay,myHour, myMin, mySec, myMilli: Word;
  myDate: TDate;
  ���, ������, �������: String;
  ���,����, �����: String;
begin
  myDate := Now;
  DecodeDate(Now, myYear, myMonth, myDay);
  DecodeTime(myDate, myHour, myMin, mySec, myMilli);

  ��� := inttostr(myYear);
  ����� := inttostr(myMonth);
  ���� := inttostr(myDay);
  ��� := inttostr(myHour);
  ������ := inttostr(myMin);
  ������� := inttostr(mySec);

  if myMonth < 10 then
    ����� := '0' + �����;
  if myDay < 10 then
    ���� := '0' + ����;
  if myHour < 10 then
    ��� := '0' + ���;
  if myMin < 10 then
    ������ := '0' + ������;
  if mySec < 10 then
    ������� := '0' + �������;
  // ***********************************
  result := ��� + ����� + ���� + ��� + ������ + �������;
end;

procedure TForm1.btNewClick(Sender: TObject);
begin
�����������������('���������');
grid.Row:=grid.RowCount-1;
editCab.Text:='���������';
//grid.Repaint;
end;

procedure TForm1.btn_otlClick(Sender: TObject);
begin
if StringReplace(new_col_edit.Text,' ','',[rfReplaceAll])='' then
begin
  ShowMessage('��� ������!');
  new_col_edit.Text:='���������';
  exit;
end;

grid.Cells[grid.Col,0]:=new_col_edit.Text;
grid.ColWidths[grid.col]:=8*grid.Cells[grid.col,0].Length;
NSaveFile.Click;
end;

procedure TForm1.chAutoClick(Sender: TObject);
begin
TabSheet2Show(nil);
end;

procedure TForm1.checkClick(Sender: TObject);
begin
��������������;
end;

procedure TForm1.editCabChange(Sender: TObject);
begin
if not editCab.Focused then exit;
NOpen.Click;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
i,s,s10:Integer;
st:String;
tex:String;
begin
myDate:=mount.Date;
grid.Cells[0,0]:='�����';
grid.Cells[1,0]:='���������';
grid.ColWidths[0]:=35;
grid.ColWidths[1]:=8*grid.Cells[1,0].Length;
new_col_edit.Text:=grid.Cells[1,0];

for I := 6 to 20 do
for s := 0 to 12 do
begin
s10:=s*5;
if s10=60 then continue;
st:=s10.ToString;
if s10<10 then st:='0'+st;
grid.RowCount:=grid.RowCount+1;
grid.Cells[0,grid.RowCount-1]:=I.ToString+':'+st;
end;

grid.FixedRows:=1;
grid.FixedCols:=1;

NLoadFile.Click;
��������������;
end;

procedure TForm1.gridClick(Sender: TObject);
var
i,j:Integer;
tex:String;
h:String;
min,min_f:Integer;
begin

new_col_edit.Text:=grid.Cells[grid.Col,0];
tex:=grid.Cells[0,grid.Row];
h:=copy(tex,0,tex.IndexOf(':'));
min:=strtoint(copy(tex,Pos(':',tex)+1));

for i:= grid.Row-1 downto 1 do
begin
if (������������(i)=false) then
begin
grid.RowHeights[i]:=grid.DefaultRowHeight;
continue;
end;
tex:=grid.Cells[0,i];
if copy(tex,0,tex.IndexOf(':'))=h then
begin
min_f:=strtoint(copy(tex,Pos(':',tex)+1));

if (min>=0)  and (min<30) then
begin
if (min_f>=30) then grid.RowHeights[i]:=0 else grid.RowHeights[i]:=grid.DefaultRowHeight;
end
else
if (min_f>0) and (min_f<30) then grid.RowHeights[i]:=0 else grid.RowHeights[i]:=grid.DefaultRowHeight;
continue;
end;

if (tex.IndexOf(':30')=-1) and (tex.IndexOf(':00')=-1) then grid.RowHeights[i]:=0 else grid.RowHeights[i]:=grid.DefaultRowHeight;

end;

for i:= grid.Row+1 to grid.RowCount-1 do
begin
tex:=grid.Cells[0,i];

if (min>=0) and (min<30) then
begin
if tex.IndexOf(':30')>0 then
begin
j:=i;
break;
end;
end else
begin
if tex.IndexOf(':00')>0 then
begin
j:=i;
break;
end;
end;


grid.RowHeights[i]:=grid.DefaultRowHeight;
end;


for i:=j+1 to grid.RowCount-1 do
begin
if (������������(i)=false) then
begin
grid.RowHeights[i]:=grid.DefaultRowHeight;
continue;
end;
tex:=grid.Cells[0,i];
if (tex.IndexOf(':00')=-1) and (tex.IndexOf(':30')=-1) then
begin
grid.RowHeights[i]:=0;
continue;
end;

grid.RowHeights[i]:=grid.DefaultRowHeight;
end;

grid.Repaint;
end;

procedure TForm1.��������;
var
val:Integer;
b:Boolean;
begin
b:=false;

if grid.Objects[grid.Col,grid.Row]=nil then
begin
case rgDate.ItemIndex of
 0:grid.Objects[grid.Col,grid.Row]:=Pointer(1);
 1:grid.Objects[grid.Col,grid.Row]:=Pointer(-1);
end;
�����(grid.Col);
b:=true;
end;

if not b then
begin
val:=Integer(grid.Objects[grid.Col,grid.Row]);

if abs(val)=2 then
begin
case rgDate.ItemIndex of
 0:grid.Objects[grid.Col,grid.Row]:=Pointer(-1);
 1:grid.Objects[grid.Col,grid.Row]:=Pointer(1);
end;
b:=true;
end;

if not b then
begin

if val=0 then
begin
case rgDate.ItemIndex of
 0:grid.Objects[grid.Col,grid.Row]:=Pointer(1);
 1:grid.Objects[grid.Col,grid.Row]:=Pointer(-1);
end;
�����(grid.Col);
end
else
case rgDate.ItemIndex of
 0:if val=1 then grid.Objects[grid.Col,grid.Row]:=Pointer(0)
 else
 begin
 if val=-1 then grid.Objects[grid.Col,grid.Row]:=Pointer(2) else grid.Objects[grid.Col,grid.Row]:=Pointer(-1);
 �����(grid.Col);
 end;
 1:if val=-1 then grid.Objects[grid.Col,grid.Row]:=Pointer(0) else
 else
 begin
 if val=1 then grid.Objects[grid.Col,grid.Row]:=Pointer(2) else grid.Objects[grid.Col,grid.Row]:=Pointer(1);
 �����(grid.Col);
 end;
end;

end;
end;


//grid.Repaint;
NSaveFile.Click;
end;

procedure TForm1.�����(col:Integer);
var
i,val:Integer;
begin
for I := 1 to grid.ColCount-1 do
begin
if i=col then continue;
if grid.Objects[i,grid.row]=nil then continue;

val:=Integer(grid.Objects[i,grid.row]);
if Abs(val)=2 then
begin
case rgDate.ItemIndex of
0:grid.Objects[i,grid.row]:=Pointer(-1);
1:grid.Objects[i,grid.row]:=Pointer(1);
end;
end else
case rgDate.ItemIndex of
0:if val=1 then grid.Objects[i,grid.row]:=Pointer(0);
1:if val=-1 then grid.Objects[i,grid.row]:=Pointer(0);
end;
end;
end;

const ����=$00B8FD8E;

procedure TForm1.gridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
  State: TGridDrawState);
var r: TRect ;
    i: integer;
    val:Integer;
begin
//Grid.Canvas.Brush.Color:=clRed;

if grid.Objects[ACol,ARow]=nil then Grid.Canvas.Brush.Color:=clWhite else
begin
val:=Integer(grid.Objects[ACol,ARow]);
if Abs(val)=2 then Grid.Canvas.Brush.Color:=���� else
case rgDate.ItemIndex of
 0:if val=1 then Grid.Canvas.Brush.Color:=���� else Grid.Canvas.Brush.Color:=clWhite;
 1:if val=-1 then Grid.Canvas.Brush.Color:=���� else Grid.Canvas.Brush.Color:=clWhite;
end;

end;

//
 if (ARow=grid.Row) or (ACol=grid.Col) then
    begin
      Grid.Canvas.Font.Style := [fsBold];
    end else
      Grid.Canvas.Font.Style := [];

 if (Grid.Canvas.Brush.Color<>����) and ((ACol=grid.Col) or (ARow=grid.Row)) then
 begin
 Grid.Canvas.Brush.Color:=$00B6FAFA;
 end;


  i:=Grid.GridLineWidth;
  r:=Grid.CellRect(ACol, ARow);
  r.left:=r.left-i;
  r.top:=r.top-i;
  r.right:=r.right+i;
  r.bottom:=r.bottom+i;
  Grid.Canvas.Pen.Color:=clGray;
  Grid.Canvas.Rectangle(r);

  Grid.Canvas.FillRect(Rect);
  Grid.Canvas.TextOut(Rect.Left,Rect.Top,Grid.Cells[Acol,Arow]);

end;

procedure TForm1.gridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if (Key=66) or (Key=98) or (Key=1042) or (Key=1074) then ��������;

end;

var
old_Row,old_Col:Integer;
procedure TForm1.gridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin

if (old_Row<>-1) and (old_Col<>-1) then
if grid.Cells[old_Col,old_Row]<>'' then
grid.Cells[old_Col,old_Row]:='';

old_Row:=ARow;
old_Col:=ACol;
if grid.Cells[old_Col,old_Row]='' then
begin
grid.Cells[old_Col,old_Row]:=grid.Cells[0,old_Row];
end;
end;

procedure TForm1.ListClick(Sender: TObject);
begin
NOpen.Click;
end;

procedure TForm1.mountClick(Sender: TObject);
begin
NOpen.Click;
end;

procedure TForm1.�����������������(���:String);
begin
grid.ColCount:=grid.ColCount+1;
grid.Cells[grid.ColCount-1,0]:=���;
grid.ColWidths[grid.ColCount-1]:=8*grid.Cells[1,0].Length;
NSaveFile.Click;
end;

procedure TForm1.N1Click(Sender: TObject);
var
i,i0:Integer;
begin
if List.ItemIndex=-1 then exit;
for I := 0 to list.Items.Count-1 do
begin
if i=List.ItemIndex then List.Checked[i]:=true else List.Checked[i]:=false;
end;
NOpen.Click;
end;

procedure TForm1.NAdd_colClick(Sender: TObject);
begin
�����������������('���������');
end;

procedure TForm1.NAdd_otdClick(Sender: TObject);
begin
�����������������(new_col_edit.Text);
end;

Type TFakeGrid=class(TCustomGrid);
//�������...
procedure RemoveColumn(SG : TStringGrid; ColNumber : integer);
var Column : integer;
begin
ColNumber := abs(ColNumber);
if ColNumber <= SG.ColCount then begin
    for Column := ColNumber to SG.ColCount - 2 do begin
       SG.Cols[Column].Assign(SG.Cols[Column + 1]);
       SG.Colwidths[Column] := SG.Colwidths[Column + 1];
    end;
    SG.ColCount := SG.ColCount - 1;
end;
end;
procedure TForm1.NDel_colClick(Sender: TObject);
begin
if (grid.ColCount=2) then exit;
if (grid.col=0) then exit;
RemoveColumn(grid,grid.col);
new_col_edit.Text:=grid.Cells[grid.Col,0];
NSaveFile.Click;
end;

procedure TForm1.NDel_otdClick(Sender: TObject);
begin
NDel_col.Click;
end;

procedure TForm1.NDel_rowClick(Sender: TObject);
begin
if (grid.col=0) then exit;
TFakeGrid(Grid).DeleteRow(Grid.row);
end;

procedure TForm1.NDIRClick(Sender: TObject);
begin
ShellExecute(Handle,
  'open',PWideChar( GetCurrentDir), nil, nil, SW_SHOWNORMAL);
end;

procedure TForm1.NhtmlClick(Sender: TObject);
begin
ShellExecute(Handle,
  'open',PWideChar( GetCurrentDir+'\cabinet.html'), nil, nil, SW_SHOWNORMAL);
end;

procedure TForm1.NLoadFileClick(Sender: TObject);
var
i:Integer;
tex:String;
begin
tex:=Form1.Caption;
i:= tex.IndexOf(' ');
tex:=Copy(tex,i+2);
LoadStringGrid(Grid,GetCurrentDir+'\'+tex+'.txt');
Form1.Caption:='������� '+tex;
for i:= 1 to grid.ColCount-1 do
grid.ColWidths[i]:=8*grid.Cells[i,0].Length;
//grid.Repaint;
end;

procedure TForm1.NModif_otdClick(Sender: TObject);
begin
grid.Cells[grid.Col,0]:=new_col_edit.Text;
NSaveFile.Click;
end;

procedure ����������������8(s,FileName:AnsiString);
var
  F : File;
  SRes : UTF8String;
begin
  //FileName := ExtractFilePath(ParamStr(0)) + Fn;
  SRes := AnsiToUtf8(S);
  AssignFile(F, FileName);
  Rewrite(F, 1);
  //��� UTF8String � Delphi 7 �������� ��� ��������� ���� AnsiString.
  //������� ����������� �����������:
  //Length(SRes) * SizeOf(AnsiChar) = Length(SRes).
  BlockWrite(F, Pointer(SRes)^, Length(SRes));
  CloseFile(F);
end;

procedure TForm1.NOpenClick(Sender: TObject);
var
l:TStringList;
i:Integer;

procedure ���������(���:String);
var
d:Integer;
sd,sm:String;
j,m:Integer;
val,day,col:Integer;
myDate : TDateTime;
x,y:integer;

procedure �������;
begin
l.Add('<div style="vertical-align: top;display:inline-block;float:left;"><table cellspacing="0" cellpadding="0" border="1">');
end;

procedure �������;
begin
  l.Add('</tr></table>');
  //l.Add('<div style="height:53px;"></div>');
end;

procedure ������������;
begin
l.Add('<td style="white:10px; font-size: 8px;"><div><button onClick="fun1();">'+���+'</button></div><br><div class="date">'+rgDate1.Items.Strings[rgDate1.ItemIndex]+'</div></td>');
end;

procedure �����;
begin
x:=x+1;

if x>5 then
begin
l.Add('</tr>');

y:=y+1;

if y>10 then
begin
y:=1;
�������;
�������;
end;
l.Add('<tr>');
x:=1;
end;

if x=1 then
begin
������������;
x:=2;
end;

l.Add('<td style="white:200px;font-weight:bold;font-size: 20px;"><div>'+editCab.text+' ���.</div><div>'+sd+'.'+sm+'.'+copy(myYear.ToString,3,2)+'</div><div>'+grid.Cells[0,j]+'</div></td>');
end;

begin
x:=0;
y:=1;
//16x10
�������;
for d:= 1 to 31 do
begin
if DaysInAMonth(myYear, myMonth)<d then break;

if (chLastNed.Visible and chLastNed.Enabled) then

 //���������
if (myMonth=1) and (d<9) then continue;
if (myMonth=2) and (d=23) then continue;
if (myMonth=3) and (d=8) then continue;
if (myMonth=5) and (d=1) then continue;
if (myMonth=5) and (d=3) then continue;
if (myMonth=5) and (d=9) then continue;
if (myMonth=6) and (d=12) then continue;
if (myMonth=8) and (d=22) then continue;
if (myMonth=11) and (d=4) then continue;

sd:=d.ToString;
if d<10 then sd:='0'+sd;
sm:=myMonth.ToString;
if myMonth<10 then sm:='0'+sm;

col:=Integer(list.Items.Objects[i]);
for j:= 1 to grid.RowCount-1 do
begin
if grid.Objects[col,j]=nil then continue;
val:=integer(grid.Objects[col,j]);
if val=0 then continue;

if abs(val)<>2 then
begin
case rgDate1.ItemIndex of
0:if val=-1 then continue;
1:if val=1 then continue;
end;
end;

myDate := EncodeDate(myYear, myMonth, d);
day:=DayOfWeek(myDate);

case rgDate1.ItemIndex of
0:if (day>1) and (day<7) then �����;
1:if day=7 then �����;
end;

end;
end;


while y<10 do
begin
//if x<10 then
//for m:= x+1 to 10 do
//l.Add('<td><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div></td>');

//l.Add('<tr>');
//for m:= 1 to 5 do
//l.Add('<td><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div></td>');
//l.Add('</tr>');
y:=y+1;
end;

�������;
l.Add('</div>');
end;

begin
DecodeDate(mount.Date, myYear, myMonth, myDay);

l:=TStringList.Create;

l.Text:='<meta charset="utf-8">'+#13#10+
'<style>'+#13#10+
'.date {font-size:12pt;}'+#13#10+
'table {display:inline-block;border-color: #008a77;}'+#13#10+
//'tr {border 1px solid black;};'+#13#10+
'td {font-size:20pt;width: 500px;vertical-align: top;text-align:center;}'+#13#10+
'button{border: 0px;width:100%;background:#fff;}'+#13#10+
'</style>'+#13#10+
'<script>'+#13#10+
'function fun1(){window.print();}'+#13#10+
'</script>'+#13#10+
'<div class="blank">';


for i:=0 to list.Count-1 do
begin
if list.Checked[i]=false then continue;
���������(list.Items.Strings[i]);
end;
l.Add('</div>');
����������������8(l.Text,GetCurrentDir+'\cabinet.html');
l.Free;

WebBrowser1.Navigate(GetCurrentDir+'\cabinet.html');
end;

procedure TForm1.NPrintClick(Sender: TObject);
begin
WebBrowser1.ExecWB(OLECMDID_PRINT, 0);
end;

procedure TForm1.NRefrech_otdClick(Sender: TObject);
var
i:Integer;
val:Integer;
begin
for I :=1 to grid.RowCount-1 do
begin
if grid.Objects[grid.Col,i]=nil then
begin
grid.Objects[grid.Col,i]:=Pointer(0);
continue;
end;

val:=Integer(grid.Objects[grid.Col,i]);
case rgDate.ItemIndex of
0:if val=1 then grid.Objects[grid.Col,i]:=Pointer(0) else grid.Objects[grid.Col,i]:=Pointer(-1);
1:if val=-1 then grid.Objects[grid.Col,i]:=Pointer(0) else grid.Objects[grid.Col,i]:=Pointer(1);
end;

end;
//grid.Repaint;
NSaveFile.Click;
end;

procedure TForm1.NRefreshClick(Sender: TObject);
var
i,j:Integer;
val:Integer;
begin
for I :=1 to grid.RowCount-1 do
for j :=1 to grid.ColCount-1 do
begin
if grid.Objects[j,i]=nil then continue;
val:=Integer(grid.Objects[j,i]);

case rgDate.ItemIndex of
0:if abs(val)=2 then grid.Objects[j,i]:=Pointer(-1) else
if val=1 then grid.Objects[j,i]:=Pointer(0);
1:if abs(val)=2 then grid.Objects[j,i]:=Pointer(1) else
if val=-1 then grid.Objects[j,i]:=Pointer(0);
end;
end;

//grid.Repaint;
NSaveFile.Click;
end;

procedure TForm1.NSaveFileClick(Sender: TObject);
var
i:Integer;
tex:String;
begin
tex:=Form1.Caption;
i:= tex.IndexOf(' ');
tex:=Copy(tex,i+2);
SaveStringGrid(Grid,GetCurrentDir+'\'+tex+'.txt');
end;

procedure TForm1.rgDate1Click(Sender: TObject);
begin
��������������(rgDate1.ItemIndex);
NOpen.Click;
end;

procedure TForm1.rgDateClick(Sender: TObject);
begin
��������������;
//grid.Repaint;
end;

procedure TForm1.��������������(key:Integer);
var
i:Integer;
begin
case rgDate1.ItemIndex of
0:chLastNed.Visible:=false;
1:chLastNed.Visible:=true;
end;

list.Clear;
for i:=1 to grid.ColCount-1 do
begin
if �������������(i,key) then continue;

list.Items.Add(grid.Cells[i,0]);
list.Items.Objects[list.Items.Count-1]:=Pointer(i);
list.Checked[list.Items.Count-1]:=true;
end;
end;

procedure TForm1.TabSheet2Show(Sender: TObject);
begin
if chAuto.Checked then
begin
mount.Date:=IncMonth(Now,1);
editCab.Text:=cbCab.Text;
rgDate1.ItemIndex:=rgDate.ItemIndex;
end;
��������������(rgDate1.ItemIndex);
NOpen.Click;
end;

procedure TForm1.test1Click(Sender: TObject);
begin
ShowMessage(DayOfTheMonth(mount.Date).ToString);
end;

function TForm1.�������������(col:Integer;key:Integer):Boolean;
var
j:Integer;
obj:TObject;
begin
for j := 1 to grid.rowcount-1 do
begin
obj:=grid.Objects[col,j];
if obj=nil then continue;

case key of
0:
if Integer(obj)=1 then
begin
result:=false;
exit;
end;
1:
if Integer(obj)=-1 then
begin
result:=false;
exit;
end;
end;

end;
result:=true;
end;

function TForm1.������������(i:Integer):Boolean;
var
j:Integer;
obj:TObject;
begin
for j := 1 to grid.ColCount-1 do
begin
obj:=grid.Objects[j,i];
if obj=nil then continue;

case rgDate.ItemIndex of
0:
if Integer(obj)=1 then
begin
result:=false;
exit;
end;
1:
if Integer(obj)=-1 then
begin
result:=false;
exit;
end;
end;

end;
result:=true;
end;

procedure TForm1.��������������;
var
i:Integer;
tex:String;

begin

for i:= 1 to grid.RowCount-1 do
begin
if ������������(i)=false then
begin
grid.RowHeights[i]:=grid.DefaultRowHeight;
continue;
end;

tex:=grid.Cells[0,i];
if (tex.IndexOf(':00')=-1) and (tex.IndexOf(':30')=-1) then
grid.RowHeights[i]:=0
else
grid.RowHeights[i]:=grid.DefaultRowHeight;
end;
end;

end.
