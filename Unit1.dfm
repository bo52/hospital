object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1050#1072#1073#1080#1085#1077#1090' 406'
  ClientHeight = 1020
  ClientWidth = 1269
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1269
    Height = 1020
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1056#1077#1076#1072#1082#1090#1086#1088
      object grid: TStringGrid
        Left = 145
        Top = 0
        Width = 1116
        Height = 992
        Align = alClient
        BevelKind = bkTile
        BevelOuter = bvRaised
        BiDiMode = bdLeftToRight
        ColCount = 2
        Ctl3D = True
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        GradientEndColor = clBlack
        GradientStartColor = clBlack
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goFixedColClick, goFixedRowClick]
        ParentBiDiMode = False
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = PopupMenu2
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        OnClick = gridClick
        OnDrawCell = gridDrawCell
        OnKeyUp = gridKeyUp
        OnSelectCell = gridSelectCell
        ColWidths = (
          64
          64)
        RowHeights = (
          15)
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 145
        Height = 992
        Align = alLeft
        Caption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
        TabOrder = 1
        object btn_otl: TButton
          Left = 2
          Top = 163
          Width = 141
          Height = 32
          Align = alTop
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100
          TabOrder = 0
          Visible = False
          OnClick = btn_otlClick
        end
        object new_col_edit: TEdit
          Left = 2
          Top = 15
          Width = 141
          Height = 21
          Align = alTop
          PopupMenu = pm_otd
          TabOrder = 1
        end
        object rgDate: TRadioGroup
          Left = 2
          Top = 67
          Width = 141
          Height = 58
          Align = alTop
          Caption = #1042#1099#1073#1086#1088' '#1044#1085#1077#1081
          Ctl3D = False
          ItemIndex = 0
          Items.Strings = (
            #1055#1086#1085#1077#1076'. - '#1055#1103#1090#1085#1080#1094#1072
            #1057#1091#1073#1073#1086#1090#1072)
          ParentCtl3D = False
          TabOrder = 2
          OnClick = rgDateClick
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 125
          Width = 141
          Height = 38
          Align = alTop
          Caption = #1050#1072#1073#1080#1085#1077#1090
          PopupMenu = pm_cabinet
          TabOrder = 3
          object cbCab: TComboBox
            Left = 2
            Top = 15
            Width = 137
            Height = 21
            Align = alClient
            Enabled = False
            ItemIndex = 0
            TabOrder = 0
            Text = '406'
            Items.Strings = (
              '406')
          end
        end
        object btNew: TButton
          Left = 2
          Top = 36
          Width = 141
          Height = 31
          Align = alTop
          Caption = #1053#1086#1074#1099#1081
          TabOrder = 4
          OnClick = btNewClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 1
      OnShow = TabSheet2Show
      object gb: TGroupBox
        Left = 0
        Top = 0
        Width = 166
        Height = 992
        Align = alLeft
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
        TabOrder = 0
        object List: TCheckListBox
          Left = 2
          Top = 296
          Width = 162
          Height = 694
          Align = alClient
          ItemHeight = 13
          PopupMenu = PopupMenu3
          TabOrder = 0
          OnClick = ListClick
        end
        object rgDate1: TRadioGroup
          Left = 2
          Top = 238
          Width = 162
          Height = 58
          Align = alTop
          Caption = #1042#1099#1073#1086#1088' '#1044#1085#1077#1081
          Ctl3D = False
          ItemIndex = 0
          Items.Strings = (
            #1055#1086#1085#1077#1076#1077#1083#1100#1085#1080#1082' - '#1055#1103#1090#1085#1080#1094#1072
            #1057#1091#1073#1073#1086#1090#1072)
          ParentCtl3D = False
          TabOrder = 1
          OnClick = rgDate1Click
        end
        object editCab: TEdit
          Left = 2
          Top = 217
          Width = 162
          Height = 21
          Align = alTop
          TabOrder = 2
          Text = '406'
          OnChange = editCabChange
        end
        object mount: TMonthCalendar
          Left = 2
          Top = 57
          Width = 162
          Height = 160
          Align = alTop
          Date = 43510.548610393520000000
          TabOrder = 3
          OnClick = mountClick
        end
        object chAuto: TCheckBox
          Left = 2
          Top = 15
          Width = 162
          Height = 17
          Align = alTop
          Caption = #1040#1074#1090#1086
          Checked = True
          State = cbChecked
          TabOrder = 4
          OnClick = chAutoClick
        end
        object chLastNed: TCheckBox
          Left = 2
          Top = 32
          Width = 162
          Height = 25
          Align = alTop
          Caption = #1055#1086#1089#1083#1077#1076#1085#1103#1103' '#1053#1077#1076#1077#1083#1103' '#1076#1086' 11:00'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
      end
      object WebBrowser1: TWebBrowser
        Left = 166
        Top = 0
        Width = 1095
        Height = 992
        Align = alClient
        TabOrder = 1
        ExplicitLeft = 416
        ExplicitTop = 48
        ExplicitWidth = 300
        ExplicitHeight = 150
        ControlData = {
          4C0000002C710000876600000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 152
    Top = 216
    object NOpen: TMenuItem
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Visible = False
      OnClick = NOpenClick
    end
    object Nhtml: TMenuItem
      Caption = 'html'
      OnClick = NhtmlClick
    end
    object NDIR: TMenuItem
      Caption = 'DIR'
      OnClick = NDIRClick
    end
    object NPrint: TMenuItem
      Caption = #1055#1077#1095#1072#1090#1100
      Visible = False
      OnClick = NPrintClick
    end
    object test1: TMenuItem
      Caption = 'test'
      OnClick = test1Click
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 768
    Top = 208
    object NNew: TMenuItem
      Caption = #1053#1086#1074#1099#1081
    end
    object NDel: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 392
    Top = 448
    object Ncol: TMenuItem
      Caption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
      object NAdd_col: TMenuItem
        Caption = #1076#1086#1073#1072#1074#1080#1090#1100
        OnClick = NAdd_colClick
      end
      object NRefrech_otd: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        OnClick = NRefrech_otdClick
      end
      object NDel_col: TMenuItem
        Caption = #1059#1076#1072#1083#1080#1090#1100
        ShortCut = 46
        OnClick = NDel_colClick
      end
    end
  end
  object pm_otd: TPopupMenu
    Left = 266
    Top = 211
    object NAdd_otd: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1054#1090#1076#1077#1083#1077#1085#1080#1077
      OnClick = NAdd_otdClick
    end
    object NDel_otd: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1054#1090#1076#1077#1083#1077#1085#1080#1077
      OnClick = NDel_otdClick
    end
    object NModif_otd: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1054#1090#1076#1077#1083#1077#1085#1080#1077
      OnClick = NModif_otdClick
    end
  end
  object pm_cabinet: TPopupMenu
    Left = 353
    Top = 365
    object NFile: TMenuItem
      Caption = 'file'
      object NSaveFile: TMenuItem
        Caption = 'Save'
        OnClick = NSaveFileClick
      end
      object NLoadFile: TMenuItem
        Caption = 'Load'
        OnClick = NLoadFileClick
      end
    end
    object NRefresh: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      OnClick = NRefreshClick
    end
  end
  object PopupMenu3: TPopupMenu
    Left = 28
    Top = 256
    object N1: TMenuItem
      Caption = #1058#1086#1083#1100#1082#1086' '#1101#1090#1086' '#1086#1090#1076#1077#1083#1077#1085#1080#1077
      OnClick = N1Click
    end
  end
end
